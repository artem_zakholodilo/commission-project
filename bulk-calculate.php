<?php

namespace App;

use App\Command\UploadCommand;
use App\Validator\Exception\ValidationException;
use App\Validator\OperationValidator;
use Monolog\Logger;

require_once 'vendor/autoload.php';

try {
    // check for input data existence
    if (empty($argv[1])) {
        throw new \InvalidArgumentException('You should provide path to a file with input data.');
    }

    $inputPath = realpath($argv[1]);
    $builder = new \DI\ContainerBuilder();
    $builder->addDefinitions(__DIR__ . 'config/app.php');
    $container = $builder->build();

    (new UploadCommand(new OperationValidator($inputPath), new Logger(), $container))->execute($inputPath);
} catch (ValidationException $validationException) {
    $validationException->log();
}