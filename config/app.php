<?php

use App\DTO\Operation;
use App\DTO\User;
use App\Strategy\CashInPrivate;
use App\Strategy\CashOutPrivate;

return [
    'operation_type' => [
        Operation::TYPE_CASH_IN => [
            'user_type' => [
                User::TYPE_NATURAL => [
                    'policy' => HasMaxLimitation::POLICY_NAME,
                ],

                User::TYPE_LEGAL => [
                    'policy' => HasMaxLimitation::POLICY_NAME,
                ]
            ]
        ],

        Operation::TYPE_CASH_OUT => [
            'user_type' => [
                User::TYPE_NATURAL => [
                    'policy' => HasFreeOfCharge::POLICY_NAME,
                ],

                User::TYPE_LEGAL => [
                    'policy' => HasMinLimitation::POLICY_NAME,
                ]
            ]
        ]
    ],
    'supported' => [
        'input' => [
            'file' => [
                'extensions' => [
                    'csv'
                ]
            ]
        ]
    ]
];