<?php

namespace App\Command;

use App\Validator\OperationValidator;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

/**
 * Class UploadCommand
 * @package App\Command
 */
class UploadCommand
{
    protected const RESULT_FAILED = 0;

    protected const RESULT_SUCCESS = 1;

    protected array $config = [];

    /**
     * @var OperationValidator
     */
    private OperationValidator $validator;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * UploadCommand constructor.
     * @param OperationValidator $validator
     */
    public function __construct(OperationValidator $validator, LoggerInterface $logger, $container)
    {
        $this->validator = $validator;
        $this->logger = $logger;
        $this->container = $container;
    }

    public function execute($filePath) // service init conainer
    {
        if (!file_exists($filePath)) {
            $this->logger->error('File does not exist');
        }

        if (($handle = fopen($filePath, "r")) === false) {
            $this->logger->error('Cannot open file');
        }

        $imported_count = 0;
        $failed_count = 0;
        $counter = 0;

        $header = fgetcsv($handle, 1000, ";");

        while (($data = fgetcsv($handle, 1000, ";")) !== false) {
            if (count($data) !== count($header)) {
                $failed_count++;
                continue;
            }


            foreach ($header as $colIndex => $locale) {
            }


            if (!$this->validator->validate()) {
                $failed_count++;
                continue;
            }

            $imported_count++;

            $counter++;
            if ($counter > 100) {
                $counter = 0;
            }
        }
        fclose($handle);

        $this->logger->info('Cities imported successfully');
        $this->logger->info("Imported rows: $imported_count");
        $this->logger->info("Failed rows:   $failed_count");

        return static::RESULT_SUCCESS;
    }
}