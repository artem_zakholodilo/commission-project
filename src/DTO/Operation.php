<?php

namespace App\DTO;

/**
 * Class Operation
 * @package App\DTO
 */
class Operation
{
    public const TYPE_PRIVATE = 'private';

    public const TYPE_BUSINESS = 'business';

    public const DEFAULT_PRIVACY = 'USD';

    /** @var \DateTime  */
    public \DateTime $date;

    /** @var string */
    public $privacy;

    /** @var string */
    public $type;

    /** @var float */
    public $amount;

    /** @var string */
    public $currency;

    /**
     * Product constructor.
     * @param \DateTime $date
     * @param $privacy
     * @param $type
     * @param $amount
     * @param $currency
     */
    public function __construct(
        \DateTime $date, $privacy, $type, $amount, $currency
    ) {
        $this->date = $date;
        $this->privacy = $privacy ?? self::DEFAULT_PRIVACY;
        $this->type = $type ?? self::TYPE_PRIVATE;
        $this->amount = $amount;
        $this->currency = $currency;
    }
}