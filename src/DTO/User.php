<?php

namespace App\DTO;

/**
 * Class User
 * @package App\DTO
 */
class User
{
    /**
     * @var int $id
     */
    public int $id;

    /**
     * User constructor.
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->id = $id;
    }
}