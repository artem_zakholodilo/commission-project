<?php

namespace App\Exception\Amount;

use App\Validator\Exception\ValidationException;

class InvalidAmount extends ValidationException
{
    public function __construct(string $amountNumber = '', $code = 0, \Throwable $previous = null)
    {
        $exceptionMessage = $this->generateExceptionMessageWithInvalidAmountNumber($amountNumber);

        parent::__construct($exceptionMessage, $code, $previous);
    }

    protected function generateExceptionMessageWithInvalidAmountNumber(string $invalidAmountNumber): string
    {
        return "Incorrect number for amount = {$invalidAmountNumber}";
    }
}