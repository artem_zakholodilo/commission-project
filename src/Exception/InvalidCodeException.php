<?php

namespace App\Validator\Exception;

class InvalidCodeException extends ValidationException
{
    public function __construct(string $currencyCode = '', $code = 0, \Throwable $previous = null)
    {
        $exceptionMessage = $this->generateExceptionMessageWithInvalidCurrency($currencyCode);

        parent::__construct($exceptionMessage, $code, $previous);
    }

    protected function generateExceptionMessageWithInvalidCurrency(string $invalidCurrencyCode): string
    {
        return "Incorrect or unsupported currency = {$invalidCurrencyCode}";
    }
}