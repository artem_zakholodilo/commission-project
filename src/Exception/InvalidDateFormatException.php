<?php

namespace App\Validator\Exception;

use Symfony\Component\Messenger\Exception\ValidationFailedException;

class InvalidDateFormatException extends ValidationFailedException
{
    public function __construct(string $date = '', $code = 0, \Throwable $previous = null)
    {
        $exceptionMessage = $this->generateExceptionMessageWithInvalidDate($date);

        parent::__construct($exceptionMessage, $code, $previous);
    }

    protected function generateExceptionMessageWithInvalidDate(string $invalidDate): string
    {
        return "Invalid date or format = {$invalidDate}";
    }
}