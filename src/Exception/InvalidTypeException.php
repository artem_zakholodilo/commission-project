<?php

namespace App\Validator\Exception;

class InvalidTypeException extends ValidationException
{
    public function __construct(string $operationType = '', $code = 0, \Throwable $previous = null)
    {
        $exceptionMessage = $this->generateExceptionMessageWithInvalidType($operationType);

        parent::__construct($exceptionMessage, $code, $previous);
    }

    protected function generateExceptionMessageWithInvalidType(string $invalidOperationType): string
    {
        return "Unsupported operation type = {$invalidOperationType}";
    }
}