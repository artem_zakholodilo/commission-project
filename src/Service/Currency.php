<?php

namespace App\Service;

use GuzzleHttp\Client;

/**
 * Class Currency
 * @package App\Service
 */
class Currency
{
    private const URL = 'https://developers.paysera.com/tasks/api/currency-exchange-rates';

    /**
     * @var Client
     */
    private $client;

    /**
     * Currency constructor.
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @return \Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getCurrency()
    {
        return $this->client->get(self::URL, []);
    }
}