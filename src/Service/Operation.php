<?php

namespace App\Service;

use App\DTO\Operation as OperationDTO;
use DI\Container;
use GuzzleHttp\Client;

/**
 * Class Operation
 * @package App\Service
 */
class Operation
{
    /**
     * @var Container
     */
    private $container;

    /**
     * @var Currency
     */
    private $currency;

    /**
     * Operation constructor.
     * @param Currency $currency
     * @param Container $container
     */
    public function __construct(Currency $currency, Container $container)
    {
        $this->currency = $currency;
        $this->container = $container;
    }

    public function getFee(OperationDTO $operation, Client $client): OperationDTO
    {
        $url = $this->container->get('url');
        $this->container->set('currency_exchange_rates', $client->get($url));

        return $operation;
    }
}