<?php

namespace App\Strategy;

/**
 * Interface OperationStrategyInterface
 * @package App\Strategy
 */
interface OperationStrategyInterface
{
    /**
     * @return void
     */
    public function fee(): void;
}