<?php

namespace App\Filter;

use App\DTO\Operation;
use App\Validator\ValidatorInterface;

/**
 * Class BaseValidator
 * @package App\Filter
 */
abstract class BaseValidator
{
    /**
     * @var Operation
     */
    private Operation $operation;


    protected $repository;

    /**
     * @var array $validators
     */
    private array $validators = [];

    /**
     * BaseValidator constructor.
     * @param Operation $operation
     * @param $repository
     */
    public function __construct(Operation $operation, $repository)
    {
        $this->operation = $operation;
        $this->repository = $repository;
        $this->validators = [];
    }

    public function appendFilter(ValidatorInterface $validator)
    {
        $validator->setData($this->request);
        $validator->setRepository($this->repository);

        $this->validators[] = $validator;

        return $this;
    }

    /**
     * Check if specific filter has been appended
     * @param mixed $filter (string) FQCN, or (object) class instance
     * @return bool
     */
    public function hasFilter($filter): bool
    {
        if(!is_string($filter) && !is_object($filter)) {
            throw new \InvalidArgumentException('Invalid filter argument type');
        }

        foreach($this->filters as $filterItem) {
            if($filterItem instanceof $filter) {
                return true;
            }
        }

        return false;
    }

    public function getFilter(string $filterClass): ?FilterInterface
    {
        foreach($this->filters as $filterItem) {
            if($filterItem instanceof $filterClass) {
                return $filterItem;
            }
        }
    }

    public function executeFilters()
    {
        if($this->filtersExecuted) {
            return;
        }

        foreach($this->filters as $filter) {
            $this->query = $filter->execute();
        }
    }

    public function getQuery()
    {
        $this->executeFilters();
        return $this->query->getQuery();
    }

    public function getQueryBuilder(): QueryBuilder
    {
        $this->executeFilters();
        return $this->query;
    }
}