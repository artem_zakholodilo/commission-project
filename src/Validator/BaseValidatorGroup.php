<?php

namespace App\Validator;

use App\Validator\GroupBase;

class BaseValidatorGroup extends GroupBase
{
    protected function createQueryBuilder($tableAlias = 't')
    {
        return $this->repository->createNotDeletedQueryBuilder('d')
            ->leftJoin(EmployeeTranslation::class, 'et', Join::WITH, 'et.translatable = e.id')
            ->leftJoin('d.schedule', 'ds')
            ->leftJoin('ds.period', 'dsp')
            ->andWhere('d.id IS NOT NULL')
            ->groupBy('d.id');
    }
}
