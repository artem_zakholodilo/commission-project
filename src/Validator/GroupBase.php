<?php

namespace App\Validator;

abstract class GroupBase
{
    protected $repository;

    private $validators;

    private $validatorsExecuted = false;

    /**
     */
    public function __construct($repository)
    {
        $this->repository = $repository;
        $this->query = $this->createQueryBuilder();
        $this->validators = [];
    }

    /**
     *
     * @param type $tableAlias
     * @return QueryBuilder
     */
    protected function createQueryBuilder($tableAlias = 't')
    {
        return $this->repository->createQueryBuilder($tableAlias);
    }

    public function appendFilter(FilterInterface $filter)
    {
        $filter->setRequest($this->request);
        $filter->setRepository($this->repository);
        $filter->setQuery($this->query);
        $filter->setGroup($this);
        $filter->retrieveValue();

        $this->filters->add($filter);

        return $this;
    }

    /**
     * Check if specific filter has been appended
     * @param mixed $filter (string) FQCN, or (object) class instance
     * @return bool
     */
    public function hasFilter($filter): bool
    {
        if(!is_string($filter) && !is_object($filter)) {
            throw new \InvalidArgumentException('Invalid filter argument type');
        }

        foreach($this->filters as $filterItem) {
            if($filterItem instanceof $filter) {
                return true;
            }
        }

        return false;
    }

    public function getFilter(string $filterClass): ?FilterInterface
    {
        foreach($this->filters as $filterItem) {
            if($filterItem instanceof $filterClass) {
                return $filterItem;
            }
        }
    }

    public function executeFilters()
    {
        if($this->filtersExecuted) {
            return;
        }

        foreach($this->filters as $filter) {
            $this->query = $filter->execute();
        }
    }

    public function getQuery()
    {
        $this->executeFilters();
        return $this->query->getQuery();
    }

    public function getQueryBuilder(): QueryBuilder
    {
        $this->executeFilters();
        return $this->query;
    }
}