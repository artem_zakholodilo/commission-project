<?php

namespace App\Validator;

/**
 * Class LocalFileSystem
 * @package App\Validator
 */
class LocalFileSystem extends BaseValidatorGroup
{
    /** {@inheritdoc} */
    public function isValidInput(string $input): bool
    {
        // check whether the passed $input is a correct path to valid file.
        if (is_file($input) && is_readable($input)) {
            // get supported input file extension list
//            $supportedFileExtensions = ConfigFactory::getInstance()->get('app.supported.input.file.extensions');
            // get extension of target file passed as an input
            $targetFileExtension = pathinfo($input, PATHINFO_EXTENSION);

            if (in_array($targetFileExtension, $supportedFileExtensions, true)) {
                return true;
            }
        }

        return false;
    }
}