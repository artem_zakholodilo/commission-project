<?php

namespace App\Validator;

/**
 * Interface ValidatorInterface
 * @package App\Validator
 */
interface ValidatorInterface
{
    public function setData($data);

    public function setRepository($repository);
}